# Changelog

## 1.3.0 - 2024-12-25

### Added

- Support for loading a user configuration file from `$HOME/.config/btrfs-desktop-notification.conf`

### Changed

- Switched from `dunst` to `libnotify` for notifications.

## 1.3.0 - 2024-12-11

### Removed

- Removed unnecessary `/usr/share/doc/`, now the PKGBUILD handles copying documentation files to the appropriate directory for Arch Linux.

## 1.2.0 - 2024-12-10

### Added

- Default terminals added for better compatibility.
- Config: Introduced `TERMINAL_ARG` to allow specifying terminal options. 

### Changed

- Simplified the bash code.
- Updated the README for improved clarity and completeness.

## 1.1.1 - 2024-11-24

### Fixed

- Corrected the description of the notification.
- README: Replaced an incorrect screenshot with two accurate screenshots.

## 1.1.0 - 2024-11-20

### Added

- Automatic reloading of changed configurations without requiring a restart.

## 1.0.1 - 2024-11-20

### Fixed

- Removed `/usr/bin/` as binary paths to ensure compatibility with non-FHS (Filesystem Hierarchy Standard) systems.
- Changed the default shebang to `#!/usr/bin/env bash` for improved compatibility.

## 1.0.0

### Added

- Initial release.

