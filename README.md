## Btrfs Desktop Notification

It provides desktop notifications for the following events:

* Booting into any read-only system or snapshot.
* Btrfs warning or error messages appearing in the dmesg log.

### Screenshot

* To test sending a kernel warning message to the `dmesg` log, run the following command:
```
# echo "<4>kernel: BTRFS warning (device sda2): checksum error at logical 26702905344 on dev /dev/sda2, physical 12417105920, root 257, inode 7845, offset 7684096, length 4096, links 1 (path: test/Desktop/amss_corrupt.bin)" > /dev/kmsg
```
![image](screenshots/1.jpg)

* To test sending a kernel error message:
```
# echo "<2>kernel: BTRFS error (device sda2): bdev /dev/sda2 errs: wr 0, rd 0, flush 0, corrupt 2638, gen 0" > /dev/kmsg
```
![image](screenshots/2.jpg)

### Requirements

* Btrfs filesystem
* `libnotify` for desktop notifications
* `systemd`, including the function `journalctl` for reading dmesg logs.


### Installation

* Arch Linux: Install from  the [AUR](https://aur.archlinux.org/packages/btrfs-desktop-notification/).

### Configuration

The program will load a user configuration file at `$HOME/.config/btrfs-desktop-notification.conf`, If it is not available, it will fall back to the default config file at `/etc/btrfs-desktop-notification.conf`.

* Enable detection when booting into a read-only root-snapshot: (yes|no)

```
DETECT_RO_SNASPHOT="yes"
```

* Set the frequency(in seconds) to check for Btrfs errors from the dmesg log.
```
TIME_INTERVAL="3"
```

* Specify a custom terminal application, such as `konsole -e` for KDE or `gnome-terminal -- bash -c` for GNOME. 
```
TERMINAL="konsole"
TERMINAL_ARG="-e"
```

* Set the log level: Warning level is 4, error level is 3.
```
LOG_LEVEL="4"
```

* Customize notification icons:
```
ERROR_ICON="/usr/share/icons/breeze-dark/status/24/data-warning.svg"
INFO_ICON="/usr/share/icons/breeze-dark/status/24/showinfo.svg"
```

### Troubleshooting

If KDE notifications are replaced with “knopwob dunst” or appear as unattractive notifications after an update, follow these steps to revert to the default KDE notifications:

* Create a directory and link the KDE notification service:
```
$ mkdir -p ~/.local/share/dbus-1/services/
$ ln -s /usr/share/dbus-1/services/org.kde.plasma.Notifications.service \
  ~/.local/share/dbus-1/services/org.kde.plasma.Notifications.service
```

* Alternatively, remove the conflicting service file:

```
$ sudo rm /usr/share/dbus-1/services/org.knopwob.dunst.service
```

### Donate

The project is free to use, but donations are welcome. Here are some ways to support:

[<img src="https://gitlab.com/Zesko/resources/-/raw/main/kofiButton.png" width=110px>](https://ko-fi.com/zeskotron) &nbsp;[<img src="https://gitlab.com/Zesko/resources/-/raw/main/PayPalButton.png" width=160px/>](https://www.paypal.com/donate/?hosted_button_id=XKP36D62AY5HY)
